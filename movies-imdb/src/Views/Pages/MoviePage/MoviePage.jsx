import React, { useState, useEffect } from "react";
import Navigation from "./Navigation/Navigation";
import Movie from "./Movie/Movie";
import Budget from "./Budget/Budget";
import Actors from "./Actors/Actors";
import { apiURL, apiKey } from "../../../Services/API/Config.json";
import PropTypes from "prop-types";

/**
 * LoadButton Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const MoviePage = ({ match, history }) => {
    /** match(prop) => match Object which now contains of the URL. */
    /** history(prop) => histyory Object contains Routing. */

    const [movie, setMovie] = useState([]);
    const [director, setDirector] = useState([]);
    const [actors, setActors] = useState([]);

    useEffect(() => {
        const movie = `${apiURL}movie/${match.params.id}?api_key=${apiKey}&language=en-us`;
        fetchData(movie);
    });

    const fetchData = (movie) => {
        fetch(movie).then((movie) =>
            movie.status === 404 ? history.push("/NotFound/404") : null
        );
        if (fetch(movie).then((movie) => movie.status === 200)) {
            fetch(movie)
                .then((movie) => movie)
                .then((movie) => movie.json())
                .then((movie) => {
                    const data = `${apiURL}movie/${match.params.id}/credits?api_key=${apiKey}`;
                    fetch(data)
                        .then((people) => people.json())
                        .then((people) => {
                            const director = people.crew.filter(
                                (people) => people.job === "Director"
                            );
                            setMovie(movie);
                            setDirector(director);
                            setActors(people.cast);
                        });
                })
                .catch((error) => console.error("Error", error));
        }
    };

    return (
        <>
            <section>
                <Navigation movieName={movie.original_title} />
                <Movie movie={movie} director={director} actors={actors} />
                <Budget movie={movie} />
                <Actors actors={actors} />
            </section>
        </>
    );
};

MoviePage.propTypes = {
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default MoviePage;
