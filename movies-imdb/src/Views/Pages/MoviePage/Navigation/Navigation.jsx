import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

/**
 * LoadButton Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const Navigation = ({ movieName }) => {
    /** movieName(prop) => with clicking the Button, There's going to be a Re-render which is going to update the state. */

    return (
        <>
            <div className="navigation">
                <div className="container">
                    <Link className="navigation__link" to="/Movies">
                        Home
                    </Link>
                    <span className="navigation__span"> / {movieName}</span>
                </div>
            </div>
        </>
    );
};

Navigation.propTypes = {
    movieName: PropTypes.any,
};

export default Navigation;
