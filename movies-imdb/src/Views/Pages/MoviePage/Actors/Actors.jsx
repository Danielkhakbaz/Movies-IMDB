import React from "react";
import { imageBaseURL } from "../../../../Services/API/Config.json";
import NoImageAvailable from "../../../../Assets/Images/NoImageAvailable.jpg";
import PropTypes from "prop-types";

/**
 * LoadButton Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const Actors = ({ actors }) => {
    /** actors(prop) => an Array of Movie's Actors. */

    const posterSize = "w154";

    return (
        <>
            <h1 className="p-4 movies__title">Actors</h1>
            <div className="p-2 movies__elements1">
                {actors.map((actor) =>
                    actor.profile_path ? (
                        <div key={actor.id} className="movie-actor">
                            <img
                                className="movies__elements--img"
                                width="150"
                                src={
                                    actor.profile_path
                                        ? `${imageBaseURL}${posterSize}${actor.profile_path}`
                                        : `${NoImageAvailable}`
                                }
                                alt="Every Single Movie Pic"
                            />
                            <h6>{actor.name}</h6>
                        </div>
                    ) : null
                )}
            </div>
        </>
    );
};

Actors.propTypes = {
    actors: PropTypes.array.isRequired,
};

export default Actors;
