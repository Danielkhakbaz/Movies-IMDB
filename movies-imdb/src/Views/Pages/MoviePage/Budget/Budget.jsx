import React from "react";
import { calcTime, convertMoney } from "../../../../Services/Utils/Convertor";
import PropTypes from "prop-types";

/**
 * LoadButton Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const Budget = ({ movie }) => {
    /** movie(prop) => the Movie object contains detail about the movie. */

    return (
        <>
            <div className="navigation">
                <div className="container-fluid">
                    <div className="row">
                        <h5 className="col-lg-4 col-md-4 col-sm-4 col-4 my-2 budget__text">
                            <i className="m-2 far fa-clock" />
                            Running Time : {calcTime(movie.runtime)}
                        </h5>
                        <h5 className="col-lg-4 col-md-4 col-sm-4 col-4 my-2 budget__text">
                            <i className="m-2 far fa-money-bill-alt" />
                            Budget : {convertMoney(movie.budget)}
                        </h5>
                        <h5 className="col-lg-4 col-md-4 col-sm-4 col-4 my-2 budget__text">
                            <i className="m-2 fas fa-funnel-dollar" />
                            Profit : {convertMoney(movie.revenue)}
                        </h5>
                    </div>
                </div>
            </div>
        </>
    );
};

Budget.propTypes = {
    movie: PropTypes.any.isRequired,
};

export default Budget;
