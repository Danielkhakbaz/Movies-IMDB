import React from "react";
import {
    imageBaseURL,
    backdropSize,
} from "../../../../Services/API/Config.json";
import PropTypes from "prop-types";

/**
 * LoadButton Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const Movie = ({ movie, director }) => {
    /** movie(prop) => the Movie object contains detail about the movie. */
    /** director(prop) => the Movie's Director. */

    const getBackgroundPoster = () => {
        if (movie.backdrop_path !== undefined) {
            const backgroundPoster = `${imageBaseURL}${backdropSize}${movie.backdrop_path}`;
            return backgroundPoster;
        }
    };
    const backgroundPoster = getBackgroundPoster();

    return (
        <>
            <div
                className="container-fluid movie"
                style={{
                    backgroundImage: `url(${backgroundPoster})`,
                }}
            >
                <div className="container movie__content">
                    <img
                        className="movie__content--img"
                        src={`${imageBaseURL}w300${movie.poster_path}`}
                        alt="img"
                    />
                    <div
                        className="p-2"
                        style={{
                            display: "flex",
                            flexDirection: "column",
                            margin: "10px",
                            color: "white",
                        }}
                    >
                        <h1
                            className="movie__row--title my-2"
                            style={{ color: "white" }}
                        >
                            {movie.original_title}
                        </h1>
                        <h6
                            className="movie__content--p my-2"
                            style={{ color: "white" }}
                        >
                            Overview
                        </h6>
                        <p className="my-2" style={{ color: "white" }}>
                            {movie.overview}
                        </p>
                        <meter
                            className="my-2"
                            min="0"
                            max="100"
                            low="40"
                            optimum="100"
                            value={movie.vote_average}
                            style={{ width: "100%" }}
                        />
                        <p className="my-2 Movie__p">
                            {director.length > 1 ? "Directors : " : "Director"}
                        </p>
                        {director.map((director) => director.name + " , ")}
                    </div>
                </div>
            </div>
        </>
    );
};

Movie.propTypes = {
    movie: PropTypes.any.isRequired,
    director: PropTypes.any.isRequired,
};

export default Movie;
