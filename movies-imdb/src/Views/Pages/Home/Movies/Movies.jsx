import React from "react";
import { Link } from "react-router-dom";
import NoImageAvailable from "../../../../Assets/Images/NoImageAvailable.jpg";
import PropTypes from "prop-types";

/**
 * Movies Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const Movies = ({ searchQuery, movies, imageURL }) => {
    /** searchQuery(prop) => if the value of searchQuery is null, We're going to have a "Load More" Button. */
    /** movies(prop) => is an Array of many objects contains of Movie Details. */
    /** imageURL(prop) => is a URL which is String contains of Movie Details. */

    return (
        <>
            <div className="movies">
                <h1 className="p-4 movies__title">
                    {searchQuery ? "Searched Movies" : "Popular Movies"}
                </h1>
                <div className="p-2 movies__elements">
                    {movies.map((movie) => (
                        <React.Fragment key={movie.id}>
                            <Link to={`/Movies/${movie.id}`}>
                                {movie.poster_path ? (
                                    <img
                                        className="movies__elements--img"
                                        src={`${imageURL}${movie.poster_path}`}
                                        alt="Every Single Movie Pic"
                                    />
                                ) : (
                                    <img
                                        className="movies__elements--img"
                                        src={NoImageAvailable}
                                        alt="No Available Pic"
                                        width="203"
                                    />
                                )}
                            </Link>
                        </React.Fragment>
                    ))}
                </div>
            </div>
        </>
    );
};

Movies.propTypes = {
    searchQuery: PropTypes.string.isRequired,
    movies: PropTypes.array.isRequired,
    imageURL: PropTypes.string.isRequired,
};

export default Movies;
