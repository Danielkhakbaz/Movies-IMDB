import React from "react";
import PropTypes from "prop-types";

/**
 * SearchBox Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const SearchBox = ({ value, onChange }) => {
    /** value(prop) => the Value of the input form. */
    /** onChange(prop) => with every single Change on the input, We're going to see value of the input. */

    return (
        <>
            <nav className="navbar">
                <input
                    className="form-control m-3 searchbox"
                    type="search"
                    value={value}
                    placeholder="Search for Movies..."
                    onChange={(e) => onChange(e.target.value)}
                />
            </nav>
        </>
    );
};

SearchBox.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default SearchBox;
