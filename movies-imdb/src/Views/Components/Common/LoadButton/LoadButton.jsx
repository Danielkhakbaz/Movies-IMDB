import React from "react";
import Spinner from "../Spinner/Spinner";
import PropTypes from "prop-types";

/**
 * LoadButton Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const LoadButton = ({ searchQuery, isLoading, onLoadButton }) => {
    /** searchQuery(prop) => if the value of searchQuery is null, We're going to have a "Load More" Button. */
    /** isLoading(prop) => with isLoading being true, We're going to have "Spinner" Component. */
    /** onLoadButton(prop) => with clicking the Button, There's going to be a Re-render which is going to update the state. */

    if (isLoading) {
        return (
            <>
                <Spinner />
            </>
        );
    }
    if (!searchQuery) {
        return (
            <>
                <div className="container">
                    <div className="row my-4 loadmore__button">
                        <div className="col-lg-1 col-md-1 col-sm-1 col-1"></div>
                        <button
                            className="col-lg-10 col-md-10 col-sm-10 col-10 btn"
                            onClick={onLoadButton}
                        >
                            Load More
                        </button>
                        <div className="col-lg-1 col-md-1 col-sm-1 col-1"></div>
                    </div>
                </div>
            </>
        );
    }

    return null;
};

LoadButton.propTypes = {
    searchQuery: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    onLoadButton: PropTypes.func.isRequired,
};

export default LoadButton;
