import React from "react";

/**
 * Spinner Component
 * @version 0.1.0
 * @author Danial Khakbaz
 */

const Spinner = () => {
    return (
        <>
            <div className="loading__icon"></div>
        </>
    );
};

export default Spinner;
