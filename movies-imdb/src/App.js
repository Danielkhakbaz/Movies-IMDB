import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Header from "./Views/Components/Main/Header/Header";
import MoviePage from "./Views/Pages/MoviePage/MoviePage";
import Home from "./Views/Pages/Home/Home";
import NotFound from "./Views/Pages/NotFound/NotFound";
import Footer from "./Views/Components/Main/Footer/Footer";

const App = () => {
    return (
        <>
            <Header />
            <Switch>
                <Route path="/Movies/:id" component={MoviePage} />
                <Route path="/Movies" component={Home} />
                <Route path="/NotFound/404" component={NotFound} />
                <Redirect exact from="/" to="/Movies" />
                <Redirect to="/NotFound/404" />
            </Switch>
            <Footer />
        </>
    );
};

export default App;
